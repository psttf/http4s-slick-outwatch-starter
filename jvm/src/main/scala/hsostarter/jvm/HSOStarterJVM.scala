package hsostarter.jvm

import cats.effect._
import cats.syntax.semigroupk._
import com.tylip.catseffect.syntax._
import hsostarter.jvm.config.{AppConfig, SlickConfig}
import hsostarter.jvm.data.db.{SlickPgProfile, Transactor}
import hsostarter.jvm.http._
import org.flywaydb.core.Flyway
import org.http4s.implicits._
import org.http4s.server.Server
import org.http4s.server.blaze.BlazeServerBuilder
import org.http4s.server.middleware.CORS
import pureconfig.ConfigSource
import pureconfig.module.catseffect.syntax._
import scribe.{Level, Logger}
import slick.basic.DatabaseConfig

import scala.concurrent.ExecutionContext

object HSOStarterJVM extends IOApp {

  override def run(args: List[String]): IO[ExitCode] =
    for {
      _ <- IO(
        Logger.root
          .clearHandlers().clearModifiers()
          .withHandler(minimumLevel = Some(Level.Debug))
          .replace(),
      )
      exitCode <- server
        .use(_ => IO.never)
        .as(ExitCode.Success)
    } yield exitCode

  private def migrateUnsafe(slickConfig: SlickConfig): Unit =
    Flyway
      .configure()
      .dataSource(
        slickConfig.db.url,
        slickConfig.db.user,
        slickConfig.db.password,
      )
      .baselineOnMigrate(true)
      .outOfOrder(true)
      .ignoreMigrationPatterns("*:missing")
      .load()
      .migrate()

  private def migrate(
    blocker: Blocker,
    slickNativeConfig: DatabaseConfig[SlickPgProfile],
  ): IO[Unit] =
    for {
      slickConfig <-
        ConfigSource
          .fromConfig(slickNativeConfig.config)
          .loadF[IO, SlickConfig](blocker)
      _ <- IO(migrateUnsafe(slickConfig))
    } yield ()

  private def initDB(blocker: Blocker): Resource[IO, Transactor[IO]] =
    for {
      slickNativeConfig <- Resource.pure[IO, DatabaseConfig[SlickPgProfile]](
        DatabaseConfig.forConfig[SlickPgProfile]("slick.dbs.default"),
      )
      _          <- migrate(blocker, slickNativeConfig).resource
      transactor <- Transactor.fromDatabaseConfig[IO](slickNativeConfig)
    } yield transactor

  private def server: Resource[IO, Server[IO]] =
    for {
      blocker    <- Blocker[IO]
      transactor <- initDB(blocker)
      appConfig  <- ConfigSource.default.loadF[IO, AppConfig](blocker).resource
      staticEndpoints = new StaticEndpoints[IO](appConfig.assets, blocker)
      httpApp = (
        staticEndpoints.endpoints() <+> HelloEndpoints.endpoints() <+>
          new NoteRoutes(transactor).routes
      ).orNotFound
      server <- BlazeServerBuilder[IO](ExecutionContext.global)
        .bindHttp(appConfig.http.port, appConfig.http.host)
        .withHttpApp(CORS(httpApp))
        .resource
    } yield server

}
