val Http4sVersion = "0.21.34"
val LogbackVersion = "1.3.11"

import sbtcrossproject.CrossPlugin.autoImport.{crossProject, CrossType}

val tylipPublic =
  "tylip-public" at "https://tylip.jfrog.io/artifactory/tylip-public/"

lazy val hsostarter =
  crossProject(JSPlatform, JVMPlatform)
    .crossType(CrossType.Full).in(file("."))
    .settings(
      organization := "ru.psttf",
      scalaVersion := "2.13.4",
      version := "0.1.0-SNAPSHOT",
      libraryDependencies ++= Seq(
        "org.typelevel" %%% "cats-effect" % "2.0.0",
        "org.typelevel" %%% "mouse" % "0.24",
        "io.circe" %%% "circe-generic" % "0.13.0",
        "io.circe" %%% "circe-literal" % "0.13.0",
        "io.circe" %%% "circe-generic-extras" % "0.13.0",
        "io.circe" %%% "circe-parser" % "0.13.0",
        "com.outr" %%% "scribe" % "2.7.10",
      ),
      scalacOptions ++= Seq(
        "-Ymacro-annotations",
        "-Xlint:-byname-implicit,_",
        "-unchecked",
        "-deprecation",
        "-feature",
        "-language:higherKinds",
      ),
      scalafmtOnCompile := !insideCI.value,
    )
    .jvmSettings(Seq(
    ))

lazy val hsostarterJS = hsostarter.js
  .enablePlugins(ScalaJSBundlerPlugin)
  .disablePlugins(RevolverPlugin)
  .settings(
    resolvers ++= Seq(
      Resolver.sonatypeRepo("releases"),
      "jitpack-tylip-proxy" at "https://nexus.tylip.com/repository/jitpack-proxy/",
      "jitpack" at "https://jitpack.io",
    ),
    libraryDependencies ++= Seq(
      "com.github.OutWatch.outwatch" %%% "outwatch" % "584f3f2c32",
    ),
    npmDependencies in Compile ++= Seq(
      "snabbdom" -> "0.7.4",
      "jquery" -> "3.3",
      "bootstrap" -> "4.3",
    ),
    webpackBundlingMode := BundlingMode.LibraryAndApplication(), // LibraryOnly() for faster dev builds
    scalaJSUseMainModuleInitializer := true,
    mainClass in Compile := Some("hsostarter.js.HSOStarterJS"),
    useYarn := true, // makes scalajs-bundler use yarn instead of npm
  )

lazy val hsostarterJVM = hsostarter.jvm
  .enablePlugins(JavaAppPackaging)
  .settings(
    resolvers += tylipPublic,
    (unmanagedResourceDirectories in Compile) += (resourceDirectory in(hsostarterJS, Compile)).value,
    mappings.in(Universal) ++= webpack.in(Compile, fullOptJS).in(hsostarterJS, Compile).value.map { f =>
      f.data -> s"assets/${f.data.getName}"
    },
    mappings.in(Universal) ++= Seq(
      (target in(hsostarterJS, Compile)).value / ("scala-" + scalaBinaryVersion.value) / "scalajs-bundler" / "main" / "node_modules" / "bootstrap" / "dist" / "css" / "bootstrap.min.css" ->
        "assets/bootstrap/dist/css/bootstrap.min.css",
      (target in(hsostarterJS, Compile)).value / ("scala-" + scalaBinaryVersion.value) / "scalajs-bundler" / "main" / "node_modules" / "jquery" / "dist" / "jquery.slim.min.js" ->
        "assets/jquery/dist/jquery.slim.min.js",
      (target in(hsostarterJS, Compile)).value / ("scala-" + scalaBinaryVersion.value) / "scalajs-bundler" / "main" / "node_modules" / "popper.js" / "dist" / "umd/popper.min.js" ->
        "assets/popper.js/dist/umd/popper.min.js",
      (target in(hsostarterJS, Compile)).value / ("scala-" + scalaBinaryVersion.value) / "scalajs-bundler" / "main" / "node_modules" / "bootstrap" / "dist" / "js/bootstrap.min.js" ->
        "assets/bootstrap/dist/js/bootstrap.min.js",
    ),
    bashScriptExtraDefines += """addJava "-Dassets=${app_home}/../assets"""",
    mainClass in reStart := Some("hsostarter.jvm.HSOStarterJVM"),
    libraryDependencies ++= Seq(
      "org.postgresql" % "postgresql" % "42.6.0",
      "com.typesafe.slick" %% "slick-hikaricp" % "3.4.1",
      "com.github.tminglei" %% "slick-pg" % "0.21.1",
      "com.github.tminglei" %% "slick-pg_circe-json" % "0.21.1",
      "org.flywaydb" % "flyway-core" % "9.22.1",
      "org.http4s" %% "http4s-blaze-server" % Http4sVersion,
      "org.http4s" %% "http4s-circe" % Http4sVersion,
      "org.http4s" %% "http4s-dsl" % Http4sVersion,
      "org.http4s" %% "http4s-blaze-client" % Http4sVersion,
      "ch.qos.logback" % "logback-classic" % LogbackVersion,
      "com.github.pureconfig" %% "pureconfig" % "0.14.1",
      "com.github.pureconfig" %% "pureconfig-cats-effect" % "0.14.1",
      "com.tylip" %% "cats-effect-syntax" % "0.1.3",
      "org.slf4j" % "slf4j-nop" % "2.0.9",
    ),
    scalacOptions += "-Wconf:cat=lint-multiarg-infix:s",
  )

disablePlugins(RevolverPlugin)

val openDev =
  taskKey[Unit]("open index-dev.html")

openDev := {
  val url = baseDirectory.value / "index-dev.html"
  streams.value.log.info(s"Opening $url in browser...")
  java.awt.Desktop.getDesktop.browse(url.toURI)
}

herokuAppName in Compile := "http4s-slick-outwatch-starter"

target in Compile := (target in(hsostarterJVM, Compile)).value
