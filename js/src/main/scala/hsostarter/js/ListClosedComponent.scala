package hsostarter.js

import cats.effect.IO
import monix.reactive.Observable
import outwatch.Handler
import outwatch.dom.VDomModifier
import outwatch.dom.dsl._

final case class ListClosedComponent private (
  eventHandler: Handler[ListClosedComponent.Event],
) {

  private def addButton =
    div(
      cls := "form-row align-items-center",
      div(
        cls := "col-auto",
        button(
          "Add",
          `type` := "submit",
          cls := "btn btn-primary mb-2",
          onClick(ListClosedComponent.AddEvent) --> eventHandler,
        ),
      ),
    )

  private def removeButton =
    div(
      cls := "form-row align-items-center",
      div(
        cls := "col-auto",
        button(
          "Remove",
          `type` := "submit",
          cls := "btn btn-primary mb-2",
          onClick(ListClosedComponent.RemoveEvent) --> eventHandler,
        ),
      ),
    )

  private def closedComponentListStream: Observable[List[ClosedComponent]] =
    eventHandler.flatScan0(List.empty[ClosedComponent]) {
      case (list, ListClosedComponent.AddEvent) =>
        for {
          closedComponent <- Observable.from(ClosedComponent.init)
        } yield closedComponent :: list
      case (_ :: tail, ListClosedComponent.RemoveEvent) =>
        Observable(tail)
      case (Nil, ListClosedComponent.RemoveEvent) =>
        Observable(Nil)
    }

  def nodeStream: Observable[VDomModifier] =
    for {
      closedComponentList <- closedComponentListStream
    } yield
      div(
        cls := "container",
        marginTop := "50px",
        Observable.from(closedComponentList).mergeMap(_.valueStream),
        addButton,
        removeButton,
        closedComponentList.map(_.node),
      )

}

object ListClosedComponent {

  sealed trait Event

  object AddEvent extends Event
  object RemoveEvent extends Event

  def init: IO[ListClosedComponent] =
    for {
      boolStream <- Handler.create[Event]
    } yield ListClosedComponent(boolStream)

}
